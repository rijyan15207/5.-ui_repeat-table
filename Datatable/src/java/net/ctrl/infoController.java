package net.ctrl;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "obj")
@SessionScoped
public class infoController 
{
    private List<Employee>employees = null;
    private Employee selectedEmployee = null;
    
    
    public void select(Employee e)
    {
        selectedEmployee = e;
    }
    
    public void update()
    {
        selectedEmployee = new Employee();
    }
    
    public void delete()
    {
        employees.remove(selectedEmployee);
    }
    
    public void add() 
    { 
        employees.add(selectedEmployee); 
    } 
    
    
    // Setter & Getter
    
    public List<Employee> getEmployees() 
    {
        return employees; 
    }

    public void setEmployees(List<Employee> employees) 
    {
        this.employees = employees;
    }
    
    
    
    @PostConstruct
    public void init()
    {
        employees = new ArrayList<Employee>();
        
        employees.add(new Employee("Md. Rijwan Hossain", "Bloody Champ", 1995));
        employees.add(new Employee("Solayman Kabir", "Sallu", 1994));
        employees.add(new Employee("Ariful Haque", "Arif", 1993));
        employees.add(new Employee("Fahad Ahmed", "pagla", 1992));
        
        selectedEmployee = new Employee();
    }

    public Employee getSelectedEmployee() {
        return selectedEmployee;
    }

    public void setSelectedEmployee(Employee selectedEmployee) {
        this.selectedEmployee = selectedEmployee;
    }
}








